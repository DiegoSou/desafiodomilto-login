package controle;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
public class Conexao{
    private Connection con = null;
    public Connection getCon(){
        return this.con;
    }
    public void setCon(Connection c){
            this.con = c;
    }
    public boolean abrirConexao(){
        try{
            Properties proprs = loadProps();
            String url = proprs.getProperty("dburl");
            this.setCon(DriverManager.getConnection(url, proprs));
            return true;
        }catch(SQLException e){
            System.out.println("[-] Erro ao conectar ao banco.");
            System.out.println(e.getMessage());
            return false;
        }
    }
    public boolean fecharConexao(){
        try{
        	if(this.con != null) {
                this.con.close();
                return true;
        	}else {
        		throw new SQLException("[-] Conex�o n�o pode ser fechada, pois possui o valor nulo.");
        	}
        }catch(SQLException e){
            throw new MyDbException(e.getMessage());
        }
    }
    
    public Properties loadProps() {
    	try(FileInputStream fs = new FileInputStream("../db.properties")){
    		Properties proprs = new Properties();
    		proprs.load(fs);
    		return proprs;
    	} catch (FileNotFoundException e) {
			throw new MyDbException(e.getMessage());
		} catch (IOException e) {
			throw new MyDbException(e.getMessage());
		}
    }
}
